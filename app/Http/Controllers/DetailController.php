<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use App\Models\Detail;

use App\Models\Thesis;
use App\Models\Contributor;
use Illuminate\Http\Request;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Session;

class DetailController extends Controller
{
 
    public function create()
    {
       

        return Inertia::render('Create');
    }

    public function store(Request $request)
    {
        $status = 0;
        $thesis = new Thesis($request->all());
        $thesis->status = $status;
        $thesis->save();
        // $thesis = Thesis::create($request->all());
        $thesisId = $thesis->id;

        


    

        // Store $thesisId in the session
        Session::put('thesis_id', $thesisId);
        
        // Create a new Detail and associate it with the Thesis
        $detail = new Detail($request->all());
        $detail->thesis_id = $thesisId;
        $detail->save();





         // Redirect to the ContributorsController's create method with the thesis_id parameter
        //  return redirect()->route('contributors.create', ['thesis_id' => $thesisId]);
        return redirect()->route('details.create');
        // return redirect()->action(
        //     [ContributorController::class, 'create'],
        //     ['thesis_id' => $thesisId]);
        // return redirect()->route('contributors.create', ['thesis_id' => $thesisId]);
        // return response()->json([
        //     'thesisId' => $thesisId,
        // ]);
    }
}
