<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use App\Models\Author;
use App\Models\Detail;
use App\Models\Thesis;
use App\Models\Article;
use App\Models\Reference;
use Illuminate\Http\Request;
use App\Models\ReferenceType;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class ReferenceController extends Controller
{
    public function getReferences(){
        $references = Reference::all();
        return response()->json($references);
    
    }




    //
    public function create()
    {
        return Inertia::render('Create');
    }


    public function store(Request $request)
    {
       // $thesisId = Session::get('thesis_id');
       $thesisId = Session::get('thesis_id');

        $reference_thesis = Thesis::create($request->all());
        $reference_thesis = $reference_thesis->id;
       

        // Session::put('thesis_id', $thesisId);




     
        $detail = new Detail($request->all());
        $detail->thesis_id = $thesisId;
        $detail->save();

        $detailId = $detail->id;


        $reference = new Reference($request->all());
        $reference->thesis_id = $thesisId;
        $reference->referenced_thesis_id =  $reference_thesis;
        $reference->details_id = $detailId; 
        $reference->save();

        $referenceId = $reference->id;

       

        $article = new Article($request->all());
        $article->thesis_id = $thesisId;
        $article->save();

        $contributors = $request->input('contributors'); // Assuming 'contributors' is the key holding the contributor data

        foreach ($contributors as $contributorData) {
            Author::create([
                'ref_id' => $referenceId,
                'fname' => $contributorData['fname'],
                'lname' => $contributorData['lname']
            ]);
        }





 

    
        
     
        // ReferenceType::create($request->all());
      
        return redirect()->route('references.create');
    }
    
};