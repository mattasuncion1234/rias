<?php

namespace App\Models;

use App\Models\Detail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Detail extends Model
{
    use HasFactory;

    protected $fillable = [
        'thesis_id',
        'title',
        'series',
        'series_num',
        'volume',
        'num_volume',
        'edition',
        'place',
        'publisher',
        'date',
        'original_date',
        'num_pages',
        'language',
        'isbn',
        'short_title',
        'url',
        'accessed',
        'archive',
        'loc_in_archive',
        'library_catalog',
        'call_number',
        'short_abstract',
        'ext_abstract',
    ];
}
