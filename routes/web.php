<?php

use Inertia\Inertia;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Application;
use App\Http\Controllers\DetailController;
use App\Http\Controllers\ThesisController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ReferenceController;
use App\Http\Controllers\ContributorController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Auth/Login', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

Route::get('/components/buttons', function () {
    return Inertia::render('Components/Buttons');
})->middleware(['auth', 'verified'])->name('components.buttons');


// Routing User Interface
Route::middleware('auth', 'verified')->group(function () {
    Route::get('/submission', [ThesisController::class, 'show'])->name('submission');
    Route::get('/submission', [ThesisController::class, 'submission'])->name('submission');
    Route::get('/home', [ThesisController::class, 'home'])->name('home');
    Route::get('/bookmark', [ThesisController::class, 'bookmark'])->name('bookmark');
    Route::get('/references', [ThesisController::class, 'references'])->name('references');
    Route::get('/generate', [ThesisController::class, 'generate'])->name('generate');
    Route::get('/assessment', [ThesisController::class, 'assessment'])->name('assessment');
    Route::get('/account', [ThesisController::class, 'account'])->name('account');

    // Route::get('/editor', [DetailsController::class, 'editor'])->name('editor');
// Route::get('/app', [BlogController::class, 'app'])->name('app');

//      
// Route::post('/details', [DetailsController::class, 'store'])->name('details.store');
// Route::post('/store-submissions', [DetailsController::class, 'store'])->name('submissions.store');

 //submissions
Route::get('/create-submissions', [ThesisController::class, 'create'])->name('submissions.create');
Route::post('/store-submissions', [ThesisController::class, 'store'])->name('submissions.store');
Route::get('/edit-submissions', [ThesisController::class, 'edit'])->name('submissions.edit');
Route::put('/update-submissions', [ThesisController::class, 'update'])->name('submissions.update');


//references
Route::get('/create-references', [ReferenceController::class, 'create'])->name('references.create');
Route::post('/references', [ReferenceController::class, 'store'])->name('references.store');
Route::get('/getReferences', [ReferenceController::class, 'getReferences'])->name('getReferences.create');


//contibutors
Route::get('/create-contributors', [ContributorController::class, 'create'])->name('contributors.create');
Route::post('/contributors', [ContributorController::class, 'store'])->name('contributors.store');
Route::get('/getStudents', [ContributorController::class, 'getStudents'])->name('students.create');
Route::get('/getEmployees', [ContributorController::class, 'getEmployees'])->name('employees.create');



// Details
Route::get('/create-details', [DetailController::class, 'create'])->name('details.create');
Route::post('/details', [DetailController::class, 'store'])->name('details.store');
});

require __DIR__ . '/auth.php';
