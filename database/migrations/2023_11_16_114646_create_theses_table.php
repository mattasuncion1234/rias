<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('theses', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('type')->comment('1=dissertation
2=capston
3=undergrad');
            $table->integer('dept_id')->nullable();
            $table->tinyInteger('status')->nullable()->comment('0=submitted | 1=approved | 2=return to student');
            $table->integer('coord_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('theses');
    }
};
